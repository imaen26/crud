<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Pengaturan
			<small>Update Pengaturan frontpage Products</small>
		</h1>
	</section>

	<section class="content">

		<div class="row">
			<div class="col-lg-6">
				
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">Pengaturan frontpage Products</h3>
					</div>
					<div class="box-body">



							<form method="post" action="<?php echo base_url('dashboard/pengaturan_update') ?>" enctype="multipart/form-data">
								<div class="box-body">
									<div class="form-group">
										<label>Title</label>
										<input type="text" name="nama" class="form-control" placeholder="Masukkan nama website.." value="">
										
									</div>

									<div class="form-group">
										<label>Short Text</label>
										<textarea name="deskripsi" class="form-control" placeholder="Masukkan deskripsi .." value=""></textarea>
										
									</div>

									<hr>

									<div class="form-group">
										<label>Gambar Produk Gas</label>
										<input type="file" name="logo">
										<small>Kosongkan jika tidak ingin mengubah logo</small>
									</div>

									<hr>
									<div class="form-group">
										<label>Gambar Produk Maintenance</label>
										<input type="file" name="logo">
										<small>Kosongkan jika tidak ingin mengubah logo</small>
									</div>

									<hr>
									<div class="form-group">
										<label>Background</label>
										<input type="file" name="logo">
										<small>Kosongkan jika tidak ingin mengubah logo</small>
									</div>

									<hr>

									
								</div>

								<div class="box-footer">
									<input type="submit" class="btn btn-success" value="Simpan">
								</div>
							</form>

					

					</div>
				</div>

			</div>
		</div>

	</section>

</div>